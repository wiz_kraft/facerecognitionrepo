import cv2
import sys
import openface
import itertools
import time
import os
import json
import argparse
import numpy as np
np.set_printoptions(precision=2)

from compare import getRep
import pandas as pd

import csv
from csv import writer

def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

# Get user supplied values
imagePath = sys.argv[1]
cascPath = "haarcascade_frontalface_default.xml"

# Create the haar cascade
faceCascade = cv2.CascadeClassifier(cascPath)

# Read the image
image = cv2.imread(imagePath)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Detect faces in the image
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30),
)

print("Found {0} faces!".format(len(faces)))

# Draw a rectangle around the faces
for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

cv2.imshow("Faces found: attempting to delete it in background", image)
cv2.waitKey(0)

imageVector = getRep(imagePath)

lines = list()
with open('data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, quoting=csv.QUOTE_NONNUMERIC, delimiter=',')
    index = 0
    entry = 0
    flag = False
    for currVector in csv_reader:
        lines.append(currVector)
        d = imageVector - currVector
        if (np.dot(d, d) == 0):
            flag = True
            entry = index
            lines.remove(currVector)
        index += 1

with open('data.csv', 'w') as writeFile:
    writer = csv.writer(writeFile)
    writer.writerows(lines)

if flag:
    lines = list()
    index = 0
    with open('identity.csv', 'r') as readFile:
        reader = csv.reader(readFile)
        for row in reader:
            lines.append(row)
            if (index == entry):
                lines.remove(row)
            index += 1

    with open('identity.csv', 'w') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(lines)