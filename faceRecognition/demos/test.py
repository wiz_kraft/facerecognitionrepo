import numpy as np
import pandas as pd

import csv
from csv import writer
import sys

def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

identity = list()
with open('identity.csv', 'r') as readFile:
    reader = csv.reader(readFile)
    for row in reader:
        identity.append(row)

print(identity)